package com.faysal.remoteunseen.room_database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NotificationsDao {

    @Query("SELECT * FROM notification")
    LiveData<List<Notifications>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNotifications(Notifications notification);

    @Update
    void updateNotifications(Notifications notification);

    @Delete
    void deleteNotifications(Notifications notification);

    @Query("DELETE FROM notification")
    public void clearAllNotification();

    @Query("SELECT * FROM notification WHERE notifyid LIKE (:notificationId) AND "+
            "package LIKE :packageName ")
    List<Notifications> getAllNotificationForSpecificUserFS(String notificationId, String packageName);

    @Query("SELECT * FROM notification WHERE package LIKE (:packageName) ")
    LiveData<List<Notifications>> getAllNotificationByPackage(String packageName);

    @Query("SELECT * FROM notification WHERE notifyid LIKE (:notificationId) AND "+
            "package LIKE :packageName AND "+ "seen_status LIKE :seenStatus")
    List<Notifications> getAllNotificationForSpecificUserUnseenMessage(String notificationId, String packageName, String seenStatus);


    @Query("SELECT * FROM notification WHERE notifyid LIKE (:notificationId)AND "+
            "package LIKE :packageName")
    LiveData<List<Notifications>> getAllNotificationForSpecificUser(String notificationId, String packageName);

    @Query("DELETE FROM notification WHERE notifyid = :userId")
    void deleteByUserId(String userId);

    @Query("UPDATE notification SET seen_status = :seenStatus WHERE notifyid LIKE :notiId AND "
            +"title LIKE :title AND "
            +"message LIKE :message AND "
            +"timestamp LIKE :timeStamp")
    void updateItem(String seenStatus, String notiId, String title, String message, String timeStamp);


    @Query("UPDATE notification SET seen_status = :seenStatus WHERE id LIKE :messageId ")
    void updateItemById(String seenStatus,int messageId);


    @Update
    void updateConversetion(Notifications notifications);


}
