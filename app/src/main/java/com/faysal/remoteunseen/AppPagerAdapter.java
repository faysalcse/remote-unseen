package com.faysal.remoteunseen;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class AppPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    List<AppFragment> fragments;


    public AppPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position).getFragment();

       /* if (position == 0) {
            return new FragmentAppView();
        } else if (position == 1){
            return new FragmentAppView();
        } else if (position == 2){
            return new FragmentAppView();
        } else {
            return new FragmentAppView();
        }*/
    }

    public void bindFragment(List<AppFragment> appFragments){
        fragments=appFragments;
        //notifyDataSetChanged();
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getTitle();
        // Generate title based on item position
        /*switch (position) {
            case 0:
                return "Messenger";
            case 1:
                return "Whatsapp";
            case 2:
                return "Imo";
            case 3:
                return "Instagram";
            default:
                return null;
        }*/
    }

}
