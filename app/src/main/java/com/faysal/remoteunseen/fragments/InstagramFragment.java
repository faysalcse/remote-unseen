package com.faysal.remoteunseen.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.remoteunseen.CustomListAdapter;
import com.faysal.remoteunseen.Model;
import com.faysal.remoteunseen.NotificationsViewModel;
import com.faysal.remoteunseen.R;
import com.faysal.remoteunseen.adapter.InstagramContactViewAdapter;
import com.faysal.remoteunseen.adapter.MessengerContactViewAdapter;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstagramFragment extends Fragment {

    CustomListAdapter adapter;
    ArrayList<Model> modelList;
    NotificationsViewModel notificationsViewModel;

    public InstagramFragment() {
        // Required empty public constructor
    }

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_for_apps, container, false);
        context=getActivity();
        modelList = new ArrayList<Model>();


        NotificationsDatabase database= NotificationsDatabase.getInstance(context);

        InstagramContactViewAdapter adapter=new InstagramContactViewAdapter(context);
        /*List<Notifications> listData=database.notificationsDao().getAll();
        Collections.reverse(listData);*/


        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        notificationsViewModel.getAllNotificationByPackage("com.instagram.android").observe(this, list -> adapter.setData(list));
       // notificationsViewModel.getAllPosts().observe(this, list -> adapter.setData(list));
        // notificationsViewModel.getAllPosts().observe(this, posts -> adapter.setData(posts));


       /* for ( Notifications notifications : listData){
            Log.d("Faysal", "onCreate: "+notifications.NotiMessage);
        }*/

        RecyclerView listNotification=view.findViewById(R.id.contact_name_list);
        listNotification.setLayoutManager(new LinearLayoutManager(context));
        listNotification.setAdapter(adapter);



        return view;
    }

}
