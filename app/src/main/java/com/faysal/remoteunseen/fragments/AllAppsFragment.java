package com.faysal.remoteunseen.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.faysal.remoteunseen.CustomListAdapter;
import com.faysal.remoteunseen.Model;
import com.faysal.remoteunseen.NotificationsViewModel;
import com.faysal.remoteunseen.R;
import com.faysal.remoteunseen.adapter.ContactViewAdapter;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllAppsFragment extends Fragment {

    CustomListAdapter adapter;
    ArrayList<Model> modelList;
    NotificationsViewModel notificationsViewModel;

    public AllAppsFragment() {
        // Required empty public constructor
    }

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_all_apps, container, false);
        context=getActivity();
        modelList = new ArrayList<Model>();


        view.findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                startActivity(intent);
            }
        });

        view.findViewById(R.id.settings).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                NotificationsDatabase database= NotificationsDatabase.getInstance(context);
                database.notificationsDao().clearAllNotification();

                return true;
            }
        });


        NotificationsDatabase database= NotificationsDatabase.getInstance(context);

        ContactViewAdapter adapter=new ContactViewAdapter(context);
        /*List<Notifications> listData=database.notificationsDao().getAll();
        Collections.reverse(listData);*/


        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        notificationsViewModel.getAllPosts().observe(this, list -> adapter.setData(list));
        // notificationsViewModel.getAllPosts().observe(this, posts -> adapter.setData(posts));


       /* for ( Notifications notifications : listData){
            Log.d("Faysal", "onCreate: "+notifications.NotiMessage);
        }*/

        RecyclerView listNotification=view.findViewById(R.id.contact_name_list);
        listNotification.setLayoutManager(new LinearLayoutManager(context));
        listNotification.setAdapter(adapter);



        return view;
    }

}
