package com.faysal.remoteunseen;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.remoteunseen.adapter.MessageViewAdapter;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;


public class DetailsView extends AppCompatActivity {

    Toolbar mToolbar;

    NotificationsViewModel notificationsViewModel;
    NotificationsDatabase database;

    String userId=null;
    String toolbarTitle = null;
    String packageName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_view);

        mToolbar=findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



        Intent intent=getIntent();



        if (intent !=null){
            userId=intent.getStringExtra("user_id");
            toolbarTitle=intent.getStringExtra("user_name");
            packageName=intent.getStringExtra("package_name");

        }


        if (toolbarTitle !=null && !TextUtils.isEmpty(toolbarTitle)){
            getSupportActionBar().setTitle(toolbarTitle);
        }


        database=NotificationsDatabase.getInstance(this);

        RecyclerView recyclerView=findViewById(R.id.messageDetails);
        MessageViewAdapter adapter=new MessageViewAdapter(this);
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        notificationsViewModel.getAllNotificationForSpecificUser(userId,packageName).observe(this, list -> adapter.setData(list));
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.message_details_view, menu);//Menu Resource, Menu

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        switch (item.getItemId()) {
            case R.id.action_delete:
                    database.notificationsDao().deleteByUserId(userId);
                    startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
