package com.faysal.remoteunseen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.faysal.remoteunseen.helper.SharedHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityChooseApp extends AppCompatActivity {


    @BindView(R.id.messagerCard)
    LinearLayout messagerCard;

    @BindView(R.id.whatsappCard)
    LinearLayout whatsappCard;

    @BindView(R.id.instagramCard)
    LinearLayout instagramCard;

    @BindView(R.id.imoCard)
    LinearLayout imoCard;


    @BindView(R.id.messengerSwitch)
    CheckBox messengerSwitch;

    @BindView(R.id.whatsappSwitch)
    CheckBox whatsappSwitch;

    @BindView(R.id.instagramSwitch)
    CheckBox instagramSwitch;

    @BindView(R.id.imoSwitch)
    CheckBox imoSwitch;

    SharedHelper helper;
    Context context;

    @BindView(R.id.saveAndChange)
    Button saveAndChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_app);
        ButterKnife.bind(this);
        context=this;

        helper=new SharedHelper();

       setCheckBox();


        messagerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messengerSwitch.isChecked()){
                    SharedHelper.putKey(context,"messenger","false");
                    messengerSwitch.setChecked(false);
                }else {
                    SharedHelper.putKey(context,"messenger","true");
                    messengerSwitch.setChecked(true);
                }
            }
        });

        whatsappCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whatsappSwitch.isChecked()){
                    SharedHelper.putKey(context,"whatsapp","false");
                    whatsappSwitch.setChecked(false);
                }else {
                    SharedHelper.putKey(context,"whatsapp","true");
                    whatsappSwitch.setChecked(true);
                }
            }
        });


        instagramCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (instagramSwitch.isChecked()){
                    SharedHelper.putKey(context,"instagram","false");
                    instagramSwitch.setChecked(false);
                }else {
                    SharedHelper.putKey(context,"instagram","true");
                    instagramSwitch.setChecked(true);
                }
            }
        });

        imoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imoSwitch.isChecked()){
                    SharedHelper.putKey(context,"imo","false");
                    imoSwitch.setChecked(false);
                }else {
                    SharedHelper.putKey(context,"imo","true");
                    imoSwitch.setChecked(true);
                }
            }
        });

        saveAndChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).toastMessage();
            }
        });


    }

    private void setCheckBox(){
        if (SharedHelper.getKey(context,"messenger").equals("true")){
            messengerSwitch.setChecked(true);
        }

        if (SharedHelper.getKey(context,"whatsapp").equals("true")){
            whatsappSwitch.setChecked(true);
        }

        if (SharedHelper.getKey(context,"instagram").equals("true")){
            instagramSwitch.setChecked(true);
        }

        if (SharedHelper.getKey(context,"imo").equals("true")){
            imoSwitch.setChecked(true);
        }

    }



}
