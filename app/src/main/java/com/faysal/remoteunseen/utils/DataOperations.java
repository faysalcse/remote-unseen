package com.faysal.remoteunseen.utils;

public class DataOperations {

    public static String dateWithMonth(String date){
        String[] simpleDate=date.split("-");
        int month=Integer.parseInt(simpleDate[1]);
        String monthName=getMonthForInt(month-1);
        return simpleDate[2]+" "+monthName+" "+simpleDate[0].substring(Math.max(simpleDate[0].length() - 2, 0));
    }

    public static String getMonthForInt(int month) {
        String[] monthNames = {"Jun", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};
        return monthNames[month];
    }
}
