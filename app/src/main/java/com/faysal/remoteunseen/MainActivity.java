package com.faysal.remoteunseen;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.faysal.remoteunseen.fragments.AllAppsFragment;
import com.faysal.remoteunseen.fragments.InstagramFragment;
import com.faysal.remoteunseen.fragments.MessangerFragment;
import com.faysal.remoteunseen.helper.SharedHelper;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    List<AppFragment> fragments;
    Context context;
    Toolbar mToolbar;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;

        mToolbar=findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        fragments=new ArrayList<>();
        fragments.add(new AppFragment("All",new AllAppsFragment()));


        if (SharedHelper.getKey(context,"messenger").equals("true")){
            fragments.add(new AppFragment("Messenger",new MessangerFragment()));
        }

        if (SharedHelper.getKey(context,"whatsapp").equals("true")){
            fragments.add(new AppFragment("Whatsapp",new FragmentAppView()));
        }

        if (SharedHelper.getKey(context,"imo").equals("true")){
            fragments.add(new AppFragment("Imo",new FragmentAppView()));
        }

        if (SharedHelper.getKey(context,"instagram").equals("true")){
            fragments.add(new AppFragment("Instagram",new InstagramFragment()));
        }

        initTabLayout(fragments);


    }



    private void initTabLayout(List<AppFragment> list){
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        // Create an adapter that knows which fragment should be shown on each page
        AppPagerAdapter adapter = new AppPagerAdapter(this, getSupportFragmentManager());
        adapter.bindFragment(list);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);
        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (adapter.getPageTitle(position).equals("Messenger")){
                    colorOperations(ContextCompat.getColor(context,R.color.messenger),ContextCompat.getColor(context,R.color.messenger));
                }else if (adapter.getPageTitle(position).equals("Whatsapp")){
                    colorOperations(ContextCompat.getColor(context,R.color.whatsapp),ContextCompat.getColor(context,R.color.whatsapp));
                }else if (adapter.getPageTitle(position).equals("Imo")){
                    colorOperations(ContextCompat.getColor(context,R.color.imo),ContextCompat.getColor(context,R.color.imo));
                }else if (adapter.getPageTitle(position).equals("Instagram")){
                    colorOperations(ContextCompat.getColor(context,R.color.instagram),ContextCompat.getColor(context,R.color.instagram));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void colorOperations(int colorPrimary,int colorAccent){
        mToolbar.setBackgroundColor(colorPrimary);
        tabLayout.setSelectedTabIndicatorColor(colorPrimary);
        tabLayout.setTabTextColors(ContextCompat.getColor(this,R.color.colorGreyDark),colorPrimary);

        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(colorPrimary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);//Menu Resource, Menu

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this,ActivityChooseApp.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void toastMessage(){
        Toast.makeText(context, "Hello World", Toast.LENGTH_SHORT).show();
    }
}
