package com.faysal.remoteunseen;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAppView extends Fragment {


    TextView textView;

    public FragmentAppView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_fragment_app_view, container, false);
        textView=view.findViewById(R.id.textView);
        return view;
    }

    public void setTextView(String txt){
        textView.setText(txt);
    }

}
